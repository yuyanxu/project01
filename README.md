% Part 1
% n=10000:100:100000;
% l=length(n);
% T=zeros(l,1);
% PI=zeros(l,1); % computed pi
% for i=1:l
%     points=rand(2,n(i));
%     x=points(1,:); y=points(2,:);
%     radius=sqrt(x.^2+y.^2);
%     l_r=length(radius);
%     c=0; tic;
%     for j=1:l_r
%         if radius(j)<=1
%             c=c+1;
%         end
%     end
%     PI(i)=c/n(i);
%     T(i)=toc;
% end
% d = abs(pi-PI*4);
% figure
% plot(n,d,'r*');
% xlabel('Points');
% ylabel('Difference between PI and true value');
% figure
% plot(T,d,'r*');
% xlabel('Time');
% ylabel('Difference');

% part 2
% j=1;
% n=1000;
% PI=[];
% error=1;
% c=0;
% points=rand(2,n);
% x=points(1,:);y=points(2,:);
% radius=(x.^2+y.^2).^(1/2);
% for i = 1:n
%     if radius(i)<=1
%         c=c+1;
%     end
% end
% PI(1)=c/n;
% while precision >= 1e-2
%     points=rand(2,n+1);
%     x=points(1,:);y=points(2,:);
%     radius = sqrt(x.^2+y.^2);
%     c=0;
%     for i=1:n
%         if radius(i)<= 1
%             c=c+1;
%         end
%     end
%     PI(j+1)=c/n;
%     error=abs(PI(j+1)-PI(j));
%     n= n+1;j=j+1;
% end
% n
% 
% Part 3
function [output] = valpi(precision)
j=1;
k=1;
s=1;
n=1000;
PI=[];
error=1;
c=0;
point=rand(2,n);
x=point(1,:);y=point(2,:);
radius=(x.^2+y.^2).^(1/2);
for i=1:n
    if radius(i)<=1
        c=c+1;
    end
end
PI(1)=c/n;
while error>=precision
    x1=[];y2=[];
    x2=[];y1=[];
    c=0;
    P=rand(2,n+1);
    x=P(1,:);y=P(2,:);
    radius=(x.^2+y.^2).^(1/2);
    for i=1:n
        if radius(i)<= 1
            x1(k)=x(i);y2(k)=y(i);
            c=c+1;k = k+1;
        else
            x2(s)=x(i);
            y1(s)=y(i);
            s=s+1;
        end
    end
    output=PI(j)
    PI(j+1)=c/n;
    error=abs(PI(j+1)-PI(j));
    n=n+1;j=j+1;
end
plot(x1,y2,'r.');
hold on;
plot(x2,y1,'b.');
end


